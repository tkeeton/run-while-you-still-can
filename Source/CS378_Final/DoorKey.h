// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickupable.h"
#include "DoorKey.generated.h"

/**
 * 
 */
UCLASS()
class CS378_FINAL_API ADoorKey : public APickupable
{
	GENERATED_BODY()
	
};
