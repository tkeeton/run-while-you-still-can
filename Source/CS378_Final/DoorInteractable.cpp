// Fill out your copyright notice in the Description page of Project Settings.


#include "DoorInteractable.h"
#include "FinalCharacter.h"
#include "DoorKey.h"

ADoorInteractable::ADoorInteractable()
{
	FrameMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FrameMesh"));
	RootComponent = FrameMesh;

	RotationOrigin = CreateDefaultSubobject<USceneComponent>(TEXT("RotationOrigin"));
	RotationOrigin->AttachToComponent(FrameMesh, FAttachmentTransformRules::KeepRelativeTransform);

	DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMesh"));
	DoorMesh->AttachToComponent(RotationOrigin, FAttachmentTransformRules::KeepRelativeTransform);

	OpenState = EDoorStateEnum::CLOSED;

	Key = nullptr;
}

void ADoorInteractable::Interact(AFinalCharacter* Character)
{
	if (!Key)
	{
		Swing();
	}
	else
	{
		if (Character->Holding
			&& Character->Holding->IsA(ADoorKey::StaticClass())
			&& Key == Cast<ADoorKey>(Character->Holding))
		{
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Green, TEXT("Unlocked!"));
			Unlock();
			Character->Holding->Destroy();
			Character->Holding = nullptr;
			Interact(Character);
		}
		else
		{
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Orange, TEXT("Locked!"));
		}
	}
}

void ADoorInteractable::Unlock()
{
	Key = nullptr;
}

FString ADoorInteractable::GetToolTip()
{
	if (OpenState == EDoorStateEnum::CLOSED
		|| OpenState == EDoorStateEnum::CLOSING)
		return "Open Door";
	else
		return "Close Door";
}
