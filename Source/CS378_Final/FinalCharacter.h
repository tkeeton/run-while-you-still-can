// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FinalCharacter.generated.h"

UCLASS()
class CS378_FINAL_API AFinalCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent;

public:
	// Sets default values for this character's properties
	AFinalCharacter();

	float MoveSpeed;
	float CrouchSpeed;
	float Speed;

	float MaxPitch;
	float MinPitch;

	bool IsCrouching;
	float StandingHeight;
	float CrouchingHeight;

	class AInteractable* CanInteract;
	bool isDisplayedToolTip;
	class APickupable* Holding;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	void MoveForward(float value);
	void MoveRight(float value);
	void Look(float x, float y);
	void Interact();
	virtual void Jump() override;
	void Crouch();

	UFUNCTION(BlueprintCallable)
	void Die();

	UFUNCTION(BlueprintImplementableEvent)
	void Pause();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Unpause();

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void DisplayDeathScreen();

	UFUNCTION(BlueprintImplementableEvent)
	void DisplayToolTip(class AInteractable* Interactable);

	UFUNCTION(BlueprintImplementableEvent)
	void RemoveToolTip();

private:
	class AInteractable* DrawRaycast();

};
