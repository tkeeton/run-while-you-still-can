// Fill out your copyright notice in the Description page of Project Settings.


#include "Trap.h"
#include "FinalCharacter.h"
#include "FollowerAI.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ATrap::ATrap()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	RootComponent = BaseMesh;

	Teeth1Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Teeth1Mesh"));
	Teeth1Mesh->AttachToComponent(BaseMesh, FAttachmentTransformRules::KeepRelativeTransform);

	Teeth2Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Teeth2Mesh"));
	Teeth2Mesh->AttachToComponent(BaseMesh, FAttachmentTransformRules::KeepRelativeTransform);

	bIsActive = true;

	TrappedMonster = nullptr;
}

// Called when the game starts or when spawned
void ATrap::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATrap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATrap::Spring(AActor* Actor)
{
	bIsActive = false;
	if (Actor->IsA(AFinalCharacter::StaticClass()))
	{
		Cast<AFinalCharacter>(Actor)->Die();
		return;
	}
	if (Actor->IsA(AFollowerAI::StaticClass()))
	{
		AFollowerAI* Monster = Cast<AFollowerAI>(Actor);
		TrappedMonster = Monster;
		Monster->GetCharacterMovement()->SetActive(false);
		GetWorld()->GetTimerManager().SetTimer(MonsterTrapped, this, &ATrap::ResetMonsterMovement, 3.0f);
	}
}

void ATrap::ResetMonsterMovement()
{
	if (TrappedMonster)
	{
		TrappedMonster->GetCharacterMovement()->SetActive(true);
	}
}
