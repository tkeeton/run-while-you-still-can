// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "Pickupable.generated.h"

/**
 * 
 */
UCLASS()
class CS378_FINAL_API APickupable : public AInteractable
{
	GENERATED_BODY()

	UPROPERTY(Category = Mesh, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;
	
public:
	APickupable();

	virtual void Interact(class AFinalCharacter* Character);

	virtual void PickUp(class AFinalCharacter* Character);

	virtual FString GetToolTip();

};
