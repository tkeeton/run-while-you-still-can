// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Perception/AIPerceptionTypes.h"
#include "FinalAIController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_FINAL_API AFinalAIController : public AAIController
{
	GENERATED_BODY()
	
	//Behavior tree component
	UBehaviorTreeComponent* BehaviorComp;

	//Blackboard Component
	UBlackboardComponent* BlackboardComp;

	//Blackboard keys
	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName LocationToGoKey;

	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName PlayerKey;

	TArray<AActor*> PatrolPoints;
	virtual void OnPossess(APawn* Pawn) override;

public:
	AFinalAIController();

	FORCEINLINE UBlackboardComponent* GetBlackboardComponent() const { return BlackboardComp; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		UAIPerceptionComponent* PerceptionComp;

	UFUNCTION(BlueprintCallable)
		void ProcessPerception(AActor* actor, FAIStimulus stimulus);
};
