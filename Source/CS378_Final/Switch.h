// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "Switch.generated.h"

/**
 * 
 */
UCLASS()
class CS378_FINAL_API ASwitch : public AInteractable
{
	GENERATED_BODY()

	UPROPERTY(Category = Mesh, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;

	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class AResponder* ResponderComponent;


public:
	ASwitch();

	virtual void Interact(class AFinalCharacter* Character) override;

};
