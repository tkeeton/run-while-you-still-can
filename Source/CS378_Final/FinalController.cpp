// Fill out your copyright notice in the Description page of Project Settings.


#include "FinalController.h"
#include "FinalCharacter.h"

void AFinalController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// Bind the different inputs for axes
	InputComponent->BindAxis("Forward", this, &AFinalController::MoveForward);
	InputComponent->BindAxis("Right", this, &AFinalController::MoveRight);
	InputComponent->BindAxis("LookX", this, &AFinalController::Look);
	InputComponent->BindAxis("LookY", this, &AFinalController::Look);

	// Bind the different inputs for actions
	InputComponent->BindAction("Interact", IE_Pressed, this, &AFinalController::Interact);
	InputComponent->BindAction("Jump", IE_Pressed, this, &AFinalController::Jump);
	InputComponent->BindAction("Crouch", IE_Pressed, this, &AFinalController::Crouch);
	InputComponent->BindAction("Pause", IE_Pressed, this, &AFinalController::Pause);
}

void AFinalController::MoveForward(float value)
{
	AFinalCharacter* character = Cast<AFinalCharacter>(GetCharacter());
	if (!character) return;
	character->MoveForward(value);
}

void AFinalController::MoveRight(float value)
{
	AFinalCharacter* character = Cast<AFinalCharacter>(GetCharacter());
	if (!character) return;
	character->MoveRight(value);
}

void AFinalController::Look(float value)
{
	AFinalCharacter* character = Cast<AFinalCharacter>(GetCharacter());
	if (!character) return;
	const float x = GetInputAxisValue("LookX");
	const float y = GetInputAxisValue("LookY");
	character->Look(x, y);
}

void AFinalController::Interact()
{
	AFinalCharacter* character = Cast<AFinalCharacter>(GetCharacter());
	if (!character) return;
	character->Interact();
}

void AFinalController::Jump()
{
	AFinalCharacter* character = Cast<AFinalCharacter>(GetCharacter());
	if (!character) return;
	character->Jump();
}

void AFinalController::Crouch()
{
	AFinalCharacter* character = Cast<AFinalCharacter>(GetCharacter());
	if (!character) return;
	character->Crouch();
}

void AFinalController::Pause()
{
	AFinalCharacter* character = Cast<AFinalCharacter>(GetCharacter());
	if (!character) return;
	character->Pause();
}
