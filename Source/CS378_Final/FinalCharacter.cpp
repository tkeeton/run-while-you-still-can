// Fill out your copyright notice in the Description page of Project Settings.


#include "FinalCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Interactable.h"

// Sets default values
AFinalCharacter::AFinalCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::KeepRelativeTransform);

	MoveSpeed = 1.0f;
	CrouchSpeed = 0.5;
	Speed = MoveSpeed;

	MaxPitch = 90.0f;
	MinPitch = -90.0f;

	IsCrouching = false;
	StandingHeight = GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	CrouchingHeight = StandingHeight / 2.0f;

	CanInteract = nullptr;
	isDisplayedToolTip = false;
	Holding = nullptr;
}

// Called when the game starts or when spawned
void AFinalCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	Speed = MoveSpeed;
}

// Called every frame
void AFinalCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CanInteract = DrawRaycast();
	if (CanInteract && !isDisplayedToolTip)
	{
		DisplayToolTip(CanInteract);
		isDisplayedToolTip = true;
	}
	else if (!CanInteract && isDisplayedToolTip)
	{
		RemoveToolTip();
		isDisplayedToolTip = false;
	}
}

// Called to bind functionality to input
void AFinalCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AFinalCharacter::MoveForward(float value)
{
	AddMovementInput(GetActorForwardVector(), value * Speed);
}

void AFinalCharacter::MoveRight(float value)
{
	AddMovementInput(GetActorRightVector(), value * Speed);
}

void AFinalCharacter::Look(float x, float y)
{
	if (x)
	{
		FRotator CharacterRotation = GetControlRotation();
		CharacterRotation.Yaw += x;
		GetController()->SetControlRotation(CharacterRotation);
	}
	if (y)
	{
		FRotator CameraRotation = CameraComponent->GetRelativeRotation();
		float Pitch = CameraRotation.Pitch;
		CameraRotation.Yaw = 0.0f;
		CameraRotation.Roll = 0.0f;
		if (Pitch <= MaxPitch)
		{
			CameraRotation.Pitch = FMath::Min(Pitch + y, MaxPitch);
		}
		if (Pitch >= MinPitch)
		{
			CameraRotation.Pitch = FMath::Max(Pitch + y, MinPitch);
		}
		CameraComponent->SetRelativeRotation(CameraRotation);
	}
}

void AFinalCharacter::Interact()
{
	if (CanInteract)
		CanInteract->Interact(this);
}

void AFinalCharacter::Jump()
{
	Super::Jump();
	if (IsCrouching)
		Crouch();
}

void AFinalCharacter::Crouch()
{
	if (IsCrouching)
	{
		GetCapsuleComponent()->SetCapsuleHalfHeight(StandingHeight);
		IsCrouching = false;
		Speed = MoveSpeed;
	}
	else
	{
		GetCapsuleComponent()->SetCapsuleHalfHeight(CrouchingHeight);
		IsCrouching = true;
		Speed = CrouchSpeed;
	}
}

void AFinalCharacter::Die()
{
	Speed = 0.0f;
	DisplayDeathScreen();
}

AInteractable* AFinalCharacter::DrawRaycast()
{
	TArray<FHitResult> Hits;
	FVector Start = CameraComponent->GetComponentLocation();
	FVector Forward = CameraComponent->GetForwardVector();
	FVector End = Start + Forward * 150.0f;
	bool DidHit = GetWorld()->LineTraceMultiByChannel(Hits, Start, End, ECC_Visibility);
	if (DidHit && GEngine)
	{
		for (FHitResult Hit : Hits)
		{
			if (Hit.GetActor()->IsA(AInteractable::StaticClass()))
			{
				return Cast<AInteractable>(Hit.GetActor());
			}
		}
	}
	return nullptr;
}
