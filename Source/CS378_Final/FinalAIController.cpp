// Fill out your copyright notice in the Description page of Project Settings.


#include "FinalAIController.h"
#include "FollowerAI.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Kismet/GameplayStatics.h"
#include "FinalCharacter.h"

AFinalAIController::AFinalAIController()
{
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
	PerceptionComp = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionComp"));

	PlayerKey = "Target";
	LocationToGoKey = "LocationToGo";
}


void AFinalAIController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);

	AFollowerAI* AICharacter = Cast<AFollowerAI>(pawn);

	if (AICharacter)
	{
		if (AICharacter->BehaviorTree->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*(AICharacter->BehaviorTree->BlackboardAsset));
		}

		//UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAIPatrolPoints::StaticClass(), PatrolPoints);

		BehaviorComp->StartTree(*(AICharacter->BehaviorTree));
	}
}

void AFinalAIController::ProcessPerception(AActor* actor, FAIStimulus stimulus) {
	AFinalCharacter* character = Cast<AFinalCharacter>(actor);

	BlackboardComp->SetValueAsObject("CharacterSpotted", character);
	BlackboardComp->SetValueAsBool("PlayerVisible", stimulus.WasSuccessfullySensed());
}