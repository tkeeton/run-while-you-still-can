// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickupable.h"
#include "FinalCharacter.h"

APickupable::APickupable()
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;
}

void APickupable::Interact(AFinalCharacter* Character)
{
	PickUp(Character);
}

void APickupable::PickUp(AFinalCharacter* Character)
{
	MeshComponent->SetSimulatePhysics(false);
	SetActorLocation(FVector(0.0f, 0.0f, -10.0f));
	Character->Holding = this;
}

FString APickupable::GetToolTip()
{
	return "Pick Up";
}
