// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378_FinalGameModeBase.h"
#include "FinalCharacter.h"
#include "FinalController.h"

ACS378_FinalGameModeBase::ACS378_FinalGameModeBase()
{
	static ConstructorHelpers::FObjectFinder<UClass> characterBPClass(TEXT("Blueprint'/Game/FinalCharacterBP.FinalCharacterBP_C'"));
	if (!characterBPClass.Object)
	{
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("FinalCharacterBP not found!"));
		DefaultPawnClass = AFinalCharacter::StaticClass();
	}
	else
	{
		DefaultPawnClass = (UClass*)characterBPClass.Object;
	}
	PlayerControllerClass = AFinalController::StaticClass();
}
