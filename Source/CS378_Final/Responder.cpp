// Fill out your copyright notice in the Description page of Project Settings.


#include "Responder.h"

// Sets default values
AResponder::AResponder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AResponder::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AResponder::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AResponder::Respond()
{

}
