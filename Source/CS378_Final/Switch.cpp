// Fill out your copyright notice in the Description page of Project Settings.


#include "Switch.h"
#include "FinalCharacter.h"
#include "Responder.h"

ASwitch::ASwitch()
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	ResponderComponent = nullptr;
}

void ASwitch::Interact(AFinalCharacter* Character)
{
	if (ResponderComponent)
		ResponderComponent->Respond();
}
