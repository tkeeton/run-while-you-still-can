// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Responder.h"
#include "Door.generated.h"

/**
 * 
 */
UCLASS()
class CS378_FINAL_API ADoor : public AResponder
{
	GENERATED_BODY()
	
	UPROPERTY(Category = Mesh, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;

public:
	ADoor();

	virtual void Respond() override;

	UFUNCTION(BlueprintImplementableEvent)
	void Open();

};
