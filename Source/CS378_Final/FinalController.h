// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FinalController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_FINAL_API AFinalController : public APlayerController
{
	GENERATED_BODY()

protected:
	void SetupInputComponent() override;

public:
	void MoveForward(float);
	void MoveRight(float);
	void Look(float);
	void Interact();
	void Jump();
	void Crouch();
	void Pause();
	
};
