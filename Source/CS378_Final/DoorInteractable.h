// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "DoorInteractable.generated.h"

UENUM(BlueprintType)
enum class EDoorStateEnum : uint8
{
	OPEN		UMETA(DisplayName: "Open"),
	CLOSED		UMETA(DisplayName: "Closed"),
	OPENING		UMETA(DisplayName: "Opening"),
	CLOSING		UMETA(DisplayName: "Closing"),
};

UCLASS()
class CS378_FINAL_API ADoorInteractable : public AInteractable
{
	GENERATED_BODY()
	
	UPROPERTY(Category = Mesh, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* FrameMesh;

	UPROPERTY(Category = Mesh, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* RotationOrigin;

	UPROPERTY(Category = Mesh, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* DoorMesh;

	UPROPERTY(Category = Gameplay, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	EDoorStateEnum OpenState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class ADoorKey* Key;

public:
	ADoorInteractable();

	virtual void Interact(class AFinalCharacter* Character) override;

	void Unlock();

	UFUNCTION(BlueprintImplementableEvent)
	void Swing();

	virtual FString GetToolTip() override;

};
